##################
#
# VPC variable definition
#
##################

variable"vpc_cidr" {
    description = "CIDR for the VPC"
    type        = string
    default     = "10.0.0.0/19"
}

variable "enable_dns_support" {
   description = "Should be true to enable DNS support in the Default VPC"
   type        = bool
   default     = true
}

variable "enable_dns_hostnames" {
   description = "Should be true to enable DNS hostnames in the Default VPC"
   type        = bool
   default     = true
}

variable "instance_tenancy" {
   description = "A tenancy option for instances launched into the VPC"
   type        = string
   default     = "default"
    
}

variable "vpc_name"{
  description = "name of vpc"
  type        = string
  default     = "Pomelo VPC"
}

variable "vpc_creator"{
 description = "creator user name of resource"
 type        = string
 default     = "PRO"
}
variable "vpc_region"{
 description = "resource of region"
 type        = string
 default     = "us-east-1"
}
variable "vpc_env"{
 description = "environment for resource"
 type        = string
 default     = "dev"
}
variable "vpc_atomation"{
 type        = string
 default     = "terraform"
}
variable "account_id"{
 description = "account id for resource"
 type        = string
 default     = "007"
}
